<!--
## CONTEXT FOR CHAT GPT IS IN THIS COMMENT BLOCK

### Neovim Configuration Overview

This Neovim configuration prioritizes modularity, flexibility, and functionality across various key development areas. Each area has a set of tools to enhance the experience, organized and managed in distinct configuration files.

#### Key Areas

1. **Plugin Management**:
   - Plugin Manager: `lazy.nvim`
   - This configuration uses `lazy.nvim` to handle plugin installation, lazy-loading, and startup optimization.
   - Plugins are organized modularly within `lua/plugins/`, where each plugin’s configuration is managed independently.
   - **Notable Configuration**:
     - `lazy-nvim.lua`: The configuration dynamically checks for the plugin path and installs `lazy.nvim` if missing. It also specifies the plugin structure under `lua/plugins/`, centralizing plugin configurations.

2. **LSP & Code Formatting**:
   - Relevant Plugins: `mason.nvim`, `nvim-lspconfig`, `none-ls` (formerly `null-ls`)
   - LSP server installation and configuration are managed with `mason.nvim` and `nvim-lspconfig`.
   - `none-ls` is used to add non-LSP formatting and linting features, expanding support for additional tools like formatters and diagnostics.
   - **Notable Configuration**:
     - `none-ls.lua`: Includes custom sources and non-LSP diagnostics, formatters, and actions. Condition-based formatting or specific file-type handling should be noted to ease expansion or debugging.

3. **Enhanced UI**:
   - Relevant Plugins: `noice.nvim`, `bufferline.nvim`, `alpha.nvim`
   - UI elements are customized to improve usability, readability, and responsiveness.
   - `noice.nvim` enhances LSP notifications and visual feedback, `bufferline.nvim` provides a visual buffer management UI, and `alpha.nvim` offers a customizable start screen.
   - **Notable Configurations**:
     - `bufferline.lua`: Document any custom icons, highlights, and tab style settings that enhance buffer visualizations and make this file a useful reference.
     - `alpha.lua`: If custom layouts, shortcuts, or project-specific options are added to the start screen, note these to facilitate quick modifications.

4. **Code Commenting**:
   - Relevant Plugins: `Comment.nvim`, `nvim-ts-context-commentstring`
   - `Comment.nvim` provides intelligent line and block commenting, with `nvim-ts-context-commentstring` adding syntax-aware support for context-sensitive commenting.
   - **Notable Configuration**:
     - `Comment.lua`: Includes custom keymaps for line and block comments and integration with Treesitter via `nvim-ts-context-commentstring`. This could benefit from documentation on key bindings and context-aware behavior.

5. **Git Integration**:
   - Relevant Plugins: `gitsigns.nvim`, `git-blame.nvim`, `git-conflict.nvim`
   - Git integration is enhanced with `gitsigns.nvim` for inline Git information, `git-blame.nvim` for line-by-line blame annotations, and `git-conflict.nvim` for managing and visualizing merge conflicts.
   - **Notable Configurations**:
     - `gitsigns.lua` and `git-blame.lua`: Custom signs, highlights, or blame formats (like initials or timestamp customizations) should be noted for clarity.

6. **Completion & Snippets**:
   - Relevant Plugins: `nvim-cmp`, `LuaSnip`
   - Autocompletion is provided by `nvim-cmp`, with support for LSP-based suggestions and snippet integration.
   - `LuaSnip` serves as the snippet engine, allowing for custom snippet definitions and integration with `nvim-cmp`.
   - **Notable Configuration**:
     - `nvim-cmp.lua`: Document source prioritization for completion sources (like LSP or snippets) to clarify any custom order or filter logic that affects suggestions.

7. **Treesitter Syntax Highlighting**:
   - Relevant Plugin: `nvim-treesitter`
   - Syntax highlighting and text manipulation are enhanced by `nvim-treesitter`, which provides language-aware features like code folding, highlighting, and text object selection.
   - **Notable Configuration**:
     - `nvim-treesitter.lua`: Treesitter modules (like `textobjects` or `folding`) should be documented, especially if their usage varies by file type. Highlight overrides should be noted for color scheme compatibility.

8. **Utility and Productivity Enhancements**:
   - Relevant Plugins: `auto-pairs.nvim`, `choosewin.nvim`, `telescope.nvim`
   - Various productivity tools, such as `auto-pairs.nvim` for auto-completion of pairs, `choosewin.nvim` for window navigation, and `telescope.nvim` for fuzzy finding, enhance the development workflow.
   - **Notable Configuration**:
     - `telescope.lua`: Custom keymaps for frequently used searches or any additional pickers (like LSP symbols) could be documented to help users leverage Telescope’s full capabilities.

9. **Development Utilities**:
   - Relevant Plugins: `utils.lua`, `actions-preview.nvim`
   - Common helper functions for formatting, linting, and navigation are centralized in `utils.lua`.
   - `actions-preview.nvim` adds a preview feature for LSP code actions, enhancing the development experience with actionable previews.
   - **Notable Configuration**:
     - `utils.lua`: Includes LSP-specific helpers and various productivity utilities. Custom functions, especially those for formatting, linting, or navigation, should be documented for ease of use and quick customization.

This modular and feature-rich setup balances performance with extensibility, making it well-suited for both general-purpose and language-specific development needs.
-->

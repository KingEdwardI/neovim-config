local lspconfig = require('lspconfig')

local M = {}

M.setup = function()
  lspconfig.lua_ls.setup({
    root_dir = function(fname)
      return lspconfig.util.root_pattern('requirements.txt', 'venv', '.git')(
        fname
      )
    end,
  })
end

return M

local lspconfig = require('lspconfig')

local M = {}

M.setup = function()
  lspconfig.rust_analyzer.setup({
    settings = {
      ['rust-analyzer'] = {
        assist = { importGranularity = 'module', importPrefix = 'self' },
        cargo = { allFeatures = true },
        checkOnSave = { command = 'clippy' },
      },
    },
    on_attach = function(_, _)
      -- Existing on_attach functionality
    end,
    capabilities = require('cmp_nvim_lsp').default_capabilities(),
  })
end

return M

local lspconfig = require('lspconfig')

local M = {}

M.setup = function()
  lspconfig.bashls.setup({
    filetypes = { 'sh', 'zsh', 'bash' },
  })
end

return M

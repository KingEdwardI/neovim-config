local lspconfig = require('lspconfig')

local M = {}

-- This tells neovim where to look for the lua binaries.
vim.api.nvim_set_var(
  'LUA_PATH',
  '/usr/local/Cellar/luarocks/3.9.2/share/lua/5.4/?.lua;/usr/local/share/lua/5.4/?.lua;/usr/local/share/lua/5.4/?/init.lua;/usr/local/lib/lua/5.4/?.lua;/usr/local/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua;/Users/edward.vetterdrake/.luarocks/share/lua/5.4/?.lua;/Users/edward.vetterdrake/.luarocks/share/lua/5.4/?/init.lua'
)
vim.api.nvim_set_var(
  'LUA_CPATH',
  '/usr/local/lib/lua/5.4/?.so;/usr/local/lib/lua/5.4/loadall.so;./?.so;/Users/edward.vetterdrake/.luarocks/lib/lua/5.4/?.so'
)

M.setup = function()
  lspconfig.lua_ls.setup({
    root_dir = function(fname)
      return lspconfig.util.root_pattern(
        'init.lua',
        'rockspec',
        '.git',
        '.luarc.json'
      )(fname)
    end,
    settings = {
      Lua = {
        runtime = {
          -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
          version = 'LuaJIT',
        },
        -- diagnostics = {
        --   -- Get the language server to recognize the `vim` global
        --   globals = { 'vim' },
        -- },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = vim.api.nvim_get_runtime_file('', true),
          checkThirdParty = false,
        },
        telemetry = {
          enable = false,
        },
        diagnostics = {
          globals = { 'vim' }, -- Recognize the `vim` global
        },
      },
    },
  })
end

return M

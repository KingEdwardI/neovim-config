local lspconfig = require('lspconfig')

local M = {}

M.setup = function()
  lspconfig.ts_ls.setup({
    settings = {
      ts_ls = {
        experimental = {
          importModuleSpecifierPreference = 'non-relative',
        },
      },
    },
    on_attach = function(client, bufnr)
      -- Manual hover binding to <C-d>
      vim.api.nvim_buf_set_keymap(
        bufnr,
        'n',
        '<Leader>dd',
        '<cmd>lua vim.lsp.buf.hover()<CR>',
        { noremap = true, silent = true }
      )
      vim.api.nvim_buf_set_keymap(
        bufnr,
        'i',
        '<C-d><C-d>',
        '<cmd>lua vim.lsp.buf.hover()<CR>',
        { noremap = true, silent = true }
      )

      vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

      local twoslash = require('twoslash-queries')
      if twoslash then
        twoslash.attach(client, bufnr)
      else
        vim.notify(
          'Warning: failed to load plugin: "twoslash-queries"',
          vim.log.levels.WARN
        )
      end

      vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
        vim.lsp.diagnostic.on_publish_diagnostics,
        { update_in_insert = false }
      )
    end,
    root_dir = function(fname)
      local cwd = lspconfig.util.root_pattern('tsconfig.json')(fname)
        or lspconfig.util.root_pattern('.eslintrc.json', '.git')(fname)
        or lspconfig.util.root_pattern('package.json', '.git/', '.zshrc')(fname)
      return cwd
    end,
    capabilities = vim.lsp.protocol.make_client_capabilities(),
    -- Todo: go through ts_ls options and nail this down
    preferences = {
      quotePreference = 'single',
      includeCompletionsForImportStatements = true,
      importModuleSpecifierPreference = 'relative',
      disableSuggestions = false,
      includeCompletionsForModuleExports = true,
      -- Todo: Not sure if this goes in this object, or the next level up
      codeActionOnSave = {
        source = {
          fixAll = true,
          removeUnused = true,
          addMissingImports = true,
          removeUnusedImports = true,
          sortImports = true,
          organizeImports = true,
        },
      },
    },
  })
end

return M

-- Global settings
-- NOTE: Setting mapleaders must happen before plugins
-- are loaded (otherwise wrong leader will be used)
vim.g.mapleader = ','
vim.g.maplocalleader = ','
vim.g.have_nerd_font = true
vim.g.python3_host_prog = '/usr/local/bin/python3'
vim.o.foldmethod = 'indent'

-- Not sure if these provider loaded things are needed
vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0
vim.g.t_Co = '256' --Support 256 colors

-- General options
vim.o.number = true
vim.o.mouse = 'a'
vim.o.showmode = false
vim.o.breakindent = true
vim.o.undofile = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.updatetime = 250
vim.o.showbreak = '└→'
vim.o.expandtab = true
vim.o.tabstop = 2
vim.o.scrolloff = 5
vim.o.foldlevelstart = 99
vim.o.undofile = true
vim.o.undodir = vim.fn.stdpath('config') .. '.nvim_undo_history'
vim.o.shiftwidth = 2
vim.o.linebreak = true
vim.o.spelllang = 'en,cjk'
vim.o.syntax = 'on'
vim.o.hidden = true --Required to keep multiple buffers open multiple buffers
vim.o.splitbelow = true --Horizontal splits will automatically be below
vim.o.splitright = true --Vertical splits will automatically be to the right
vim.o.termguicolors = true
vim.o.showmode = false --We don't need to see things like -- INSERT -- anymore
vim.o.updatetime = 300 --Faster completion
vim.o.timeoutlen = 500 --By default timeoutlen is 1000 ms
vim.o.cursorline = true
vim.o.hlsearch = true
vim.opt.iskeyword:append('-')

-- Buffer options
vim.b.expandtab = true --Converts tabs to spaces
vim.b.smartindent = true --Makes indenting smart

-- Window Options
vim.w.cursorline = true --Enable highlighting of the current line
vim.w.signcolumn = 'yes' --Always show the signcolumn, otherwise it would shift the text each time

-- Setting filetype to bash for zsh files for better syntax highlighting
vim.api.nvim_create_autocmd('FileType', {
  pattern = 'zsh',
  callback = function()
    vim.bo.filetype = 'bash'
  end,
})

-- Auto-configure folding when LSP attaches
vim.api.nvim_create_autocmd('LspAttach', {
  callback = function()
    -- Default fold settings
    vim.o.foldmethod = 'indent'
    vim.o.foldlevel = 99 -- Start with all folds open

    -- Check if the active LSP supports foldingRange
    local active_clients = vim.lsp.get_clients()
    for _, client in ipairs(active_clients) do
      if client.supports_method('textDocument/foldingRange') then
        vim.o.foldmethod = 'expr'
        vim.o.foldexpr = 'v:lua.vim.lsp.foldexpr()'
        return
      end
    end
  end,
})

-- I'm going to disable this for now and see if it's needed.
-- Disable things to help startup time
-- local disabled_built_ins = {
--   'netrw',
--   'netrwPlugin',
--   'netrwSettings',
--   'netrwFileHandlers',
--   'gzip',
--   'zip',
--   'zipPlugin',
--   'tar',
--   'tarPlugin',
--   'getscript',
--   'getscriptPlugin',
--   'vimball',
--   'vimballPlugin',
--   '2html_plugin',
--   'logipat',
--   'rrhelper',
--   'spellfile_plugin',
-- }
-- for _, plugin in pairs(disabled_built_ins) do
--   vim.g['loaded_' .. plugin] = 1
-- end

-- Potential options that need to be vetted one at a time to make sure I like them

-- Preview substitutions live, as you type!
-- vim.opt.inccommand = 'split'

-- TODO: Add a method to ensure that various things are installed before continuing, such as external packages from npm that will be needed. i.e. bash-language-server.

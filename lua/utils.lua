local Utils = {}

Utils.dump = function(o)
  if type(o) == 'table' then
    local s = '{ '
    for k, v in pairs(o) do
      if type(k) ~= 'number' then
        k = '"' .. k .. '"'
      end
      s = s .. '[' .. k .. '] = ' .. Utils.dump(v) .. ','
    end
    return s .. '} '
  else
    return tostring(o)
  end
end

Utils.buf_map = function(bufnr, mode, lhs, rhs, opts)
  opts = vim.tbl_extend('force', { noremap = true, silent = true }, opts or {})
  vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts)
end

Utils.deep_merge = function(t1, t2)
  for k, v in pairs(t2) do
    if type(v) == 'table' and type(t1[k]) == 'table' then
      Utils.deep_merge(t1[k], v)
    else
      t1[k] = v
    end
  end
end

return Utils

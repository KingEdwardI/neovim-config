return function()
  vim.cmd([[
  let s:chocobo_color1 = 0
  let s:chocobo_color_2 = 0
  let s:chocobo_color_3 = 0
  let s:chocobo_color_4 = 0
  let s:chocobo_color_5 = 0
  let s:chocobo_color_6 = 0
  let s:chocobo_color_7 = 0
  let s:chocobo_color_8 = 0
  let s:chocobo_color_9 = 0
  let s:chocobo_color_10 = 0
  let s:chocobo_color_11 = 0
  let s:chocobo_color_12 = 0
  let s:chocobo_color_13 = 0
  let s:chocobo_color_14 = 0
  let s:chocobo_color_15 = 0
  let s:chocobo_color_16 = 0
  let s:chocobo_color_17 = 0
  let s:chocobo_color_18 = 0
  let s:chocobo_color_19 = 0

function! HighlightChocobo()
  highlight ChocoboGold guifg=#AB893F
  highlight ChocoboYellow guifg=#E7BD3E
  highlight ChocoboPaleYellow guifg=#C7B290
  highlight ChocoboPurple guifg=#5E4683
  highlight ChocoboBlue guifg=#6486AC
  highlight ChocoboOrange guifg=#AB5729

  let s:chocobo_color1 = matchadd('ChocoboYellow', '#')
  let s:chocobo_color_2 = matchadd('ChocoboGold', '(')
  let s:chocobo_color_3 = matchadd('ChocoboGold', '%')
  let s:chocobo_color_4 = matchadd('ChocoboGold', '\.')
  let s:chocobo_color_5 = matchadd('ChocoboYellow', '/#')
  let s:chocobo_color_6 = matchadd('ChocoboYellow', '#\*')
  let s:chocobo_color_7 = matchadd('ChocoboPaleYellow', '@')
  let s:chocobo_color_8 = matchadd('ChocoboPurple', '/\*\*')
  let s:chocobo_color_9 = matchadd('ChocoboBlue', '//')
  let s:chocobo_color_10 = matchadd('ChocoboPurple', '\*\*')
  let s:chocobo_color_11 = matchadd('ChocoboOrange', '/\\(')
  let s:chocobo_color_12 = matchadd('ChocoboOrange', ' \*\*')
  let s:chocobo_color_13 = matchadd('ChocoboOrange', '\*\* ')
  let s:chocobo_color_14 = matchadd('ChocoboOrange', '/(')
  let s:chocobo_color_15 = matchadd('ChocoboOrange', ' ///// ')
  let s:chocobo_color_16 = matchadd('ChocoboOrange', '((//')
  let s:chocobo_color_17 = matchadd('ChocoboOrange', '\*////')
  let s:chocobo_color_18 = matchadd('ChocoboOrange', '//// ')
  let s:chocobo_color_19 = matchadd('ChocoboOrange', '((//')
endfunction

function! RemoveChocoboHighlight()
  if &ft != 'alpha'
    try
      call matchdelete(s:chocobo_color1)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_2)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_3)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_4)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_5)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_6)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_7)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_8)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_9)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_10)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_11)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_12)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_13)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_14)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_15)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_16)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_17)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_18)
    catch
    endtry
    try
      call matchdelete(s:chocobo_color_19)
    catch
    endtry
  endif
endfunction

autocmd FileType alpha call HighlightChocobo()
autocmd FileType * if &ft!="alpha" | call RemoveChocoboHighlight() | endif
]])

  return [[
  ]] .. [[
  ]] .. [[
  ]] .. [[ 
              ########((%%%%%%%%%.              
          #####@@@@(((###%                      
        ####/**//@@(((%#  #%                    
        ####@@@**@@(((####                      
        ##(((((((((#######                      
        ((         %#%##              #%        
                   ####(           %#*          
                 #########     ####  /##        
               ######################*          
               #########((((#########*          
               **#####(#(((((((#(**             
        @@     /(           (((((    (@@        
          /////                  ((//           
            *////            @@////             
    ]]
end

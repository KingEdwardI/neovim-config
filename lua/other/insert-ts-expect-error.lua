local api = vim.api

local function insert_ts_expect_error()
  -- Get current cursor position
  local cursor_pos = api.nvim_win_get_cursor(0)
  -- Current line
  local line = cursor_pos[1]
  -- Get current line text
  local line_text = api.nvim_get_current_line()
  -- Calculate indentation level
  local indent = line_text:match('^%s*') or ''
  -- Insert `// @ts-expect-error` above the current line
  api.nvim_buf_set_lines(
    0,
    line - 1,
    line - 1,
    false,
    { indent .. '// @ts-expect-error' }
  )
end

return insert_ts_expect_error

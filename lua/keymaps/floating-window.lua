local wk = require('which-key')

local function resize_float()
  local width = tonumber(vim.fn.input('Width: '))
  local height = tonumber(vim.fn.input('Height: '))

  if width and height then
    local win = vim.api.nvim_get_current_win()
    local config = vim.api.nvim_win_get_config(win)
    config.width = width
    config.height = height
    vim.api.nvim_win_set_config(win, config)
  else
    vim.notify('Invalid input for width or height')
  end
end

local function move_float()
  local row = tonumber(vim.fn.input('Row: '))
  local col = tonumber(vim.fn.input('Col: '))

  if row and col then
    local win = vim.api.nvim_get_current_win()
    local config = vim.api.nvim_win_get_config(win)
    config.row = row
    config.col = col
    vim.api.nvim_win_set_config(win, config)
  else
    vim.notify('Invalid input for row or col')
  end
end

wk.add({
  group = 'Window Controls',
  {
    '<Leader>wr',
    resize_float,
    desc = 'Resize floating window',
  },
  {
    '<Leader>wm',
    move_float,
    desc = 'Move floating window',
  },
})

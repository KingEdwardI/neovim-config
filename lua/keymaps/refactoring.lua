local wk = require('which-key')

wk.add({
  group = 'Refactoring',
  -- Visual mode mappings
  {
    mode = 'v',
    {
      '<Leader>re',
      '<Cmd>Refactor extract<CR>',
      desc = 'Extract selection into common method',
    },
    {
      '<Leader>rf',
      '<Cmd>Refactor extract_to_file<CR>',
      desc = 'Extract selection into a separate file',
    },
    {
      '<Leader>rv',
      '<Cmd>Refactor extract_var<CR>',
      desc = 'Extract selected value to variable',
    },
    {
      '<Leader>ri',
      '<Cmd>Refactor inline_var<CR>',
      desc = 'Replace instances of a variable with its value',
    },
    {
      '<Leader>rI',
      '<Cmd>Refactor inline_func<CR>',
      desc = 'Replace instances of a method with its value',
    },
    {
      '<Leader>rr',
      '<Cmd>lua require("telescope").extensions.refactoring.refactors()<CR>',
      desc = 'Open refactoring selection menu',
    },
  },
  -- Normal mode mappings
  {
    '<Leader>re',
    '<Cmd>Refactor extract_block<CR>',
    desc = 'Extract selection into common method',
  },
  {
    '<Leader>rf',
    '<Cmd>Refactor extract_block_to_file<CR>',
    desc = 'Extract selection into a separate file',
  },
  {
    '<Leader>ri',
    '<Cmd>Refactor inline_var<CR>',
    desc = 'Replace instances of a variable with its value',
  },
  {
    '<Leader>rr',
    '<Cmd>lua require("telescope").extensions.refactoring.refactors()<CR>',
    desc = 'Open refactoring selection menu',
  },
})

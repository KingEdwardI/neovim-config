local wk = require('which-key')
local builtin = require('telescope.builtin')

wk.add({
  group = 'Telescope',
  {
    '<Leader>fz',
    '<Cmd>Telescope<CR>',
    desc = 'Find anything',
  },
  {
    '<Leader>ff',
    '<Cmd>Telescope find_files hidden=true no_ignore=true<CR>',
    desc = 'Find files (in git)',
  },
  {
    '<Leader>fF',
    builtin.resume,
    desc = 'Resume previous file search',
  },
  {
    '<Leader>fR',
    builtin.resume,
    desc = 'Resume previous search',
  },
  {
    '<Leader>fr',
    builtin.lsp_references,
    desc = 'Find lsp references',
  },
  {
    '<Leader>fi',
    builtin.lsp_implementations,
    desc = 'Find lsp implementations',
  },
  {
    '<Leader>fd',
    builtin.lsp_definitions,
    desc = 'Find lsp definitions',
  },
  {
    '<Leader>fD',
    builtin.diagnostics,
    desc = 'Display diagnostics for open buffer',
  },
  {
    '<Leader>ft',
    builtin.live_grep,
    desc = 'Find by text',
  },
  {
    '<Leader>fT',
    builtin.resume,
    desc = 'Resume previous text search',
  },
  {
    '<Leader>fw',
    builtin.grep_string,
    desc = 'Search for word under cursor',
  },
  {
    '<Leader>fb',
    builtin.buffers,
    desc = 'Find buffer',
  },
  {
    '<Leader>fh',
    builtin.command_history,
    desc = 'View command history',
  },
  {
    '<Leader>fH',
    builtin.search_history,
    desc = 'View search history',
  },
  {
    '<Leader>fq',
    builtin.quickfix,
    desc = 'View quickfix menu',
  },
  {
    '<Leader>fQ',
    builtin.quickfixhistory,
    desc = 'View quickfix history',
  },
})

local wk = require('which-key')

wk.add({
  group = 'Markdown Preview',
  {
    '<Leader>md',
    '<Cmd>MarkdownPreviewToggle<CR>',
    desc = 'Toggle markdown preview',
    mode = 'n',
  },
})

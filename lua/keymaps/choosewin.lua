local wk = require('which-key')

wk.add({
  { '<Leader>gw', '<Plug>(choosewin)', desc = 'Choose window' },
})

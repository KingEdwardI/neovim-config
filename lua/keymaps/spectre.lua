local wk = require('which-key')

wk.add({
  group = 'Spectre (search and replace)',
  {
    '<Leader>ss',
    '<Cmd>lua require("spectre").open()<CR>',
    desc = 'Open Spectre',
  },
  {
    '<Leader>sw',
    '<Cmd>lua require("spectre").open_visual({select_word=true})<CR>',
    desc = 'Open Visual/Search Word',
  },
  {
    '<Leader>sf',
    '<Cmd>lua require("spectre").open_file_search()<CR>',
    desc = 'Open File Search',
  },
})

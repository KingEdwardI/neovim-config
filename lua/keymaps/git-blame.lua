local wk = require('which-key')

wk.add({
  group = 'Git actions',
  {
    '<Leader>gb',
    '<Cmd>GitBlameToggle<CR>',
    desc = 'Toggle git blame',
  },
})

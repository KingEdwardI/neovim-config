local wk = require('which-key')

wk.add({
  -- Remappings for Dvorak & personal preference
  {
    mode = 'n',
    { 'j', 'n' },
    { 'J', 'N' },
    { 'k', 'J' },
    { '<C-Y', '"+yy' },
    -- wrap all lines at the first space after the 80th column.
    { '<C-f>f', '<Cmd>%s/\\(.\\{80\\}.\\{-}\\s\\)/\\1\\r/g<CR>' },
    {
      group = 'Go to',
      {
        '<Leader>gd',
        '<cmd>lua vim.lsp.buf.definition()<CR>',
        desc = 'Go to definition',
      },
      {
        '<Leader>gi',
        '<cmd>lua vim.lsp.buf.definition()<CR>',
        desc = 'Go to definition',
      },
      {
        '<Leader>gr',
        '<cmd>lua vim.lsp.buf.definition()<CR>',
        desc = 'Go to definition',
      },
      {
        '<Leader>gD',
        '<cmd>lua vim.lsp.buf.definition()<CR>',
        desc = 'Go to definition',
      },
      { 'gt', '<Cmd>bnext<CR>', desc = 'Go to next buffer' },
      { 'gT', '<Cmd>bprevious<CR>', desc = 'Go to previous buffer' },
      { 'gc', '<Cmd>bnext | bd #<CR>', desc = 'Go to next buffer' },
      { 'gC', '<Cmd>bprevious | bd #<CR>', desc = 'Go to previous buffer' },
    },
    { '<Leader>qq', '<Cmd>qa<CR>', desc = 'Close all buffers' },
    { '<Leader>qQ', '<Cmd>qa<CR>', desc = 'Force close all buffers' },
    {
      '<Leader>rn',
      '<cmd>lua vim.lsp.buf.rename()<CR>',
      desc = 'Rename variable',
    },
    {
      '<C-w><C-w>',
      function()
        -- Start by cycling to the next window
        vim.cmd('wincmd w')

        -- Check if we landed on a 'notify' window
        local filetype = vim.bo.filetype
        if filetype == 'notify' then
          -- If it's a notify window, cycle again
          vim.cmd('wincmd w')
        end
      end,
      desc = 'Switch window (upgraded)',
      noremap = true,
    },
  },

  {
    mode = 'v',
    { '<C-y>', '"+y' },
  },

  {
    mode = 'i',
    { 'hh', '<Esc>', noremap = true },
  },

  {
    mode = { 'n', 'v' },
    { '-', '$' },
    { '_', '^' },
  },

  {
    mode = { 'n', 'v', 'o' },
    { 'h', '<Left>' },
    { 't', '<Up>' },
    { 'T', '8<Up>' },
    { 'n', '<Down>' },
    { 'N', '8<Down>' },
    { 's', '<Right>' },
  },
})

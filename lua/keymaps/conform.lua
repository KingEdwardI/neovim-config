local wk = require('which-key')

wk.add({
  group = 'Linting/Formatting',
  {
    '<Leader>lf',
    function()
      require('conform').format({
        async = true,
        lsp_fallback = true,
      })
      vim.api.nvim_create_autocmd('BufWritePre', {
        pattern = '*',
        callback = function(args)
          require('conform').format({ bufnr = args.buf })
        end,
      })
    end,
    desc = 'Format current document',
  },
})

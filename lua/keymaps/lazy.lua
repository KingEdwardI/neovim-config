local wk = require('which-key')

wk.add({
  group = 'Lazy actions',
  {
    '<Leader>Ll',
    '<Cmd>Lazy<CR>',
    desc = 'Open Lazy',
  },
})

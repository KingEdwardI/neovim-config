local wk = require('which-key')

wk.add({
  group = 'Actions menu',
  {
    '<Leader><Leader>',
    function()
      require('actions-preview').code_actions()
    end,
    desc = 'Toggle line comment',
    mode = { 'v', 'n' },
  },
})

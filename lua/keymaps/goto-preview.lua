local wk = require('which-key')

wk.add({
  group = 'Hover preview menus',
  {
    '<Leader>hd',
    "<cmd>lua require('goto-preview').goto_preview_definition()<CR>",
    desc = 'Preview Definition',
  },
  {
    '<Leader>ht',
    "<cmd>lua require('goto-preview').goto_preview_type_definition()<CR>",
    desc = 'Preview Type Definition',
  },
  {
    '<Leader>hi',
    "<cmd>lua require('goto-preview').goto_preview_implementation()<CR>",
    desc = 'Preview implementation',
  },
  {
    '<Leader>hr',
    "<cmd>lua require('goto-preview').goto_preview_references()<CR>",
    desc = 'Preview References',
  },
  {
    '<Leader>hc',
    "<cmd>lua require('goto-preview').close_all_win()<CR>",
    desc = 'Close Preview Windows',
  },
})

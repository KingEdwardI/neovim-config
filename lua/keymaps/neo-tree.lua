local wk = require('which-key')

wk.add({
  group = 'File Explorer',
  {
    '<Leader>tt',
    '<Cmd>Neotree reveal<CR>',
    desc = 'Open current file in explorer',
  },
  { '<Leader>tc', '<Cmd>Neotree close<CR>', desc = 'Close explorer' },
})

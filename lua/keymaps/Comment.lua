local wk = require('which-key')

wk.add({
  group = 'Commenting actions',
  {
    '<Leader>cc',
    '<Plug>(comment_toggle_linewise_visual)',
    desc = 'Toggle line comment',
    mode = 'v',
  },
  {
    '<Leader>cb',
    'Toggle block comment',
    desc = 'Toggle block comment',
  },
  {
    '<Leader>ct',
    'Add comment above current line',
    desc = 'Add comment above current line',
  },
  {
    '<Leader>cn',
    'Add comment below current line',
    desc = 'Add comment below current line',
  },
})

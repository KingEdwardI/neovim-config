local wk = require('which-key')

wk.add({
  group = 'Trouble diagnostic window',
  {
    '<Leader>xx',
    '<Cmd>Trouble diagnostics toggle<CR>',
    desc = 'Toggle Trouble menu',
  },
  {
    '<Leader>xw',
    '<Cmd>Trouble workspace_diagnostics<CR>',
    desc = 'Open workspace diagnostics',
  },
  {
    '<Leader>xd',
    '<Cmd>Trouble document_diagnostics<CR>',
    desc = 'Open document diagnostics',
  },
  {
    '<Leader>xl',
    '<Cmd>Trouble loclist<CR>',
    desc = 'Todo: figure out what this is',
  },
  {
    '<Leader>xf',
    '<Cmd>Trouble quickfix<CR>',
    desc = 'Quickfix problems',
  },
  {
    '<Leader>xn',
    '<Cmd>lua vim.diagnostic.goto_next({ float = false })<CR>',
    desc = 'Go to next diagnostic',
  },
  {
    '<Leader>xN',
    '<Cmd>lua vim.diagnostic.goto_prev({ float = false })<CR>',
    desc = 'Go to previous diagnostic',
  },
})

return {
  'stevearc/conform.nvim',
  config = function()
    require('conform').setup({
      formatters_by_ft = {
        lua = { 'stylua' },
        -- Conform will run multiple formatters sequentially
        python = { 'isort', 'black' },
        -- Use a sub-list to run only the first available formatter
        javascript = { 'eslint_d', 'eslint' },
        typescript = { 'eslint_d', 'eslint' },
        typescriptreact = { 'eslint_d', 'eslint' },
      },
    })
  end,
}

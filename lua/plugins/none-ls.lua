local insert_ts_expect_error = require('other.insert-ts-expect-error')

return {
  'nvimtools/none-ls.nvim',
  dependencies = {
    'nvimtools/none-ls-extras.nvim',
  },
  config = function()
    local null_ls = require('null-ls')
    null_ls.setup({
      sources = {
        -- TypeScript and JavaScript
        require('none-ls.diagnostics.eslint_d'),
        require('none-ls.code_actions.eslint_d'),
      },
      -- debounce = 100,
    })
    null_ls.register({
      name = 'Custom Actions',
      method = { null_ls.methods.CODE_ACTION },
      filetypes = { '_all' },
      generator = {
        fn = function()
          local actions = {}
          local filetype = vim.bo.filetype

          if filetype == 'typescript' or filetype == 'typescriptreact' then
            table.insert(actions, {
              title = 'Insert @ts-expect-error',
              action = function()
                insert_ts_expect_error()
              end,
            })
          end
          return actions
        end,
      },
    })
  end,
}

return {
  'nvim-telescope/telescope.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      build = 'make',
    },
    'sharkdp/fd',
    'nvim-treesitter/nvim-treesitter',
    'nvim-tree/nvim-web-devicons',
  },
  config = function()
    local telescope = require('telescope')
    local actions = require('telescope.actions')
    local config = require('telescope.config')

    -- File and text search in hidden files and directories
    ---@diagnostic disable-next-line: deprecated
    local vimgrep_arguments = { unpack(config.values.vimgrep_arguments) }
    table.insert(vimgrep_arguments, '--hidden')

    telescope.setup({
      defaults = {
        wrap_results = true,
        path_display = { 'smart' },
        vimgrep_arguments = vimgrep_arguments,
        mappings = {
          i = {
            ['<Esc>'] = actions.close,
          },
        },
        file_ignore_patterns = {
          'node_modules',
          '.undo_history',
          '.nx',
          '.yalc',
        },
        cache_pickers = {
          limit_entries = 5000,
        },
        -- pickers = {
        --   find_files = {
        --     find_command = {
        --       'rg',
        --       '--no-ignore',
        --       '--files',
        --       '--hidden',
        --       '-g',
        --       '!.git',
        --     },
        --   },
        -- },
        layout_strategy = 'vertical',
        debounce = 200,
        sorting_strategy = 'ascending',
      },
      extensions = {
        fzf = {
          fuzzy = true, -- false will only do exact matching
          override_generic_sorter = true, -- override the generic sorter
          override_file_sorter = true, -- override the file sorter
          case_mode = 'smart_case', -- or "ignore_case" or "respect_case"
          -- the default case_mode is "smart_case"
        },
      },
    })
    -- require('telescope').load_extension('fzf')
  end,
}

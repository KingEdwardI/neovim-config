return {
  'Bryley/neoai.nvim',
  requires = { 'MunifTanjim/nui.nvim' },
  cmd = {
    'NeoAI',
    'NeoAIOpen',
    'NeoAIClose',
    'NeoAIToggle',
    'NeoAIContext',
    'NeoAIContextOpen',
    'NeoAIContextClose',
    'NeoAIInject',
    'NeoAIInjectCode',
    'NeoAIInjectContext',
    'NeoAIInjectContextCode',
  },
  config = function()
    local OPEN_AI_ACCESS_TOKEN = require('AccessTokens').GPT4
    require('neoai').setup({
      ui = {
        output_popup_text = 'NeoAI',
        input_popup_text = 'Prompt',
        width = 30, -- As percentage eg. 30%
        output_popup_height = 80, -- As percentage eg. 80%
        submit = '<C-Enter>', -- Key binding to submit the prompt
      },
      models = {
        {
          name = 'openai',
          -- model = 'gpt-4-1106-preview',
          model = 'gpt-3.5-turbo-0125',
          params = nil,
        },
      },
      register_output = {
        ['g'] = function(output)
          return output
        end,
        ['c'] = require('neoai.utils').extract_code_snippets,
      },
      inject = {
        cutoff_width = 75,
      },
      prompts = {
        -- context_prompt = function(context)
        --   return "Hey, I'd like to provide some context for future "
        --     .. 'messages. Here is the code/text that I want to refer '
        --     .. 'to in our upcoming conversations:\n\n'
        --     .. context
        -- end,
      },
      mappings = {
        ['select_up'] = '<C-u>',
        ['select_down'] = '<C-d>',
      },
      open_ai = {
        api_key = {
          value = OPEN_AI_ACCESS_TOKEN,
          -- `get` is is a function that retrieves an API key, can be used to override the default method.
          -- get = function() ... end

          -- Here is some code for a function that retrieves an API key. You can use it with
          -- the Linux 'pass' application.
          -- get = function()
          --     local key = vim.fn.system("pass show openai/mytestkey")
          --     key = string.gsub(key, "\n", "")
          --     return key
          -- end,
        },
      },
      shortcuts = {
        -- {
        --   name = 'textify',
        --   key = '<leader>at',
        --   desc = 'fix text with AI',
        --   use_context = true,
        --   prompt = [[
        --           Please rewrite the text to make it more readable, clear,
        --           concise, and fix any grammatical, punctuation, or spelling
        --           errors
        --       ]],
        --   modes = { 'v' },
        --   strip_function = nil,
        -- },
        -- {
        --   name = 'gitcommit',
        --   key = '<leader>ag',
        --   desc = 'generate git commit message',
        --   use_context = false,
        --   prompt = function()
        --     return [[
        --               Using the following git diff generate a consise and
        --               clear git commit message, with a short title summary
        --               that is 75 characters or less:
        --           ]] .. vim.fn.system('git diff --cached')
        --   end,
        --   modes = { 'n' },
        --   strip_function = nil,
        -- },
      },
    })
  end,
}

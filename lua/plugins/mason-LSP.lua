return {
  {
    'williamboman/mason.nvim',
    build = ':MasonUpdate', -- Optional, but keeps installed tools up-to-date
    config = function()
      require('mason').setup()
    end,
  },
  {
    'williamboman/mason-lspconfig.nvim',
    dependencies = {
      'neovim/nvim-lspconfig',
      'marilari88/twoslash-queries.nvim',
    },
    config = function()
      require('mason-lspconfig').setup({
        ensure_installed = { 'lua_ls', 'ts_ls', 'rust_analyzer' },
      })

      require('lsp.bash').setup()
      require('lsp.lua').setup()
      require('lsp.rust').setup()
      require('lsp.python').setup()
      require('lsp.typescript').setup()
    end,
  },
}

return {
  'radenling/vim-dispatch-neovim',
  dependencies = 'tpope/vim-dispatch',
  cmd = { 'Dispatch', 'Make', 'Focus', 'Start' },
  event = 'VeryLazy'
}

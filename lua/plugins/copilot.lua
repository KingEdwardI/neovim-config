return {
  'zbirenbaum/copilot.lua',
  cmd = 'Copilot',
  event = 'InsertEnter',
  config = function()
    require('copilot').setup({
      layout = {
        position = 'right',
        ratio = 0.4,
      },
    })
  end,
}

return {
  'folke/noice.nvim',
  event = 'VeryLazy',
  config = function()
    require('noice').setup({
      lsp_doc_border = true,
      lsp = {
        override = {
          ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
          ['vim.lsp.util.stylize_markdown'] = true,
          ['cmp.entry.get_documentation'] = true,
        },
        hover = { enabled = false, trigger = false, luasnip = false },
        signature = {
          auto_open = { enabled = false },
        },
        progress = {
          enabled = true,
          throttle = 500,
          delay = {
            enabled = true,
            done = 0,
          },
        },
      },
      presets = {
        bottom_search = true,
      },
      views = {
        cmdline_popup = {
          position = {
            row = 5,
            col = '50%',
          },
        },
        popupmenu = {
          relative = 'editor',
          position = {
            row = 5,
            col = '50%',
          },
        },
      },
    })
  end,
  dependencies = {
    -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
    'MunifTanjim/nui.nvim',
    -- OPTIONAL:
    --   `nvim-notify` is only needed, if you want to use the notification view.
    --   If not available, we use `mini` as the fallback
    'rcarriga/nvim-notify',
  },
}

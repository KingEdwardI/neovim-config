local chocobo_banner = require('other.chocobo')
local theme_utils = require('other.alpha-theme')
local mru = theme_utils.mru

local function info_text()
  local datetime = os.date(' %d-%m-%Y')
  local version = vim.version()
  local nvim_version_info = '   v'
    .. version.major
    .. '.'
    .. version.minor
    .. '.'
    .. version.patch

  return '        ' .. datetime .. '   ' .. nvim_version_info
end

local info = {
  type = 'text',
  val = info_text(),
  opts = {
    hl = 'DevIconVim',
    position = 'center',
  },
}

local heading = {
  type = 'text',
  val = chocobo_banner(),
  opts = {
    position = 'center',
    hl = 'AlphaHeading',
  },
}

local header = {
  type = 'group',
  val = {
    heading,
    { type = 'padding', val = 1 },
    info,
  },
  opts = {
    position = 'center',
  },
}

local buttons = function(dashboard)
  return {
    type = 'group',
    val = {
      dashboard.button('f', ' Find File', '<Cmd>Telescope find_files<CR>'),
      dashboard.button('g', '? Live Grep', '<Cmd>Telescope live_grep<CR>'),
      dashboard.button('t', '¤  Tree', '<Cmd>Neotree<CR>'),
      dashboard.button('u', '  Update plugins', '<Cmd>Lazy update<CR>'),
      dashboard.button('q', '✖  Quit NVIM', '<Cmd>qa<CR>'),
    },
    opts = {
      position = 'center',
      spacing = 1,
    },
  }
end

local recent_files = {
  type = 'group',
  val = {
    {
      type = 'text',
      val = 'Recent files',
      opts = {
        hl = 'String',
        shrink_margin = false,
        position = 'center',
      },
    },
    { type = 'padding', val = 1 },
    {
      type = 'group',
      val = function()
        return {
          mru(1, vim.fn.getcwd(), 10),
        }
      end,
      opts = { position = 'center' },
    },
  },
  opts = {
    position = 'center',
  },
}

return {
  'goolord/alpha-nvim',
  dependencies = { 'nvim-tree/nvim-web-devicons' },
  lazy = false,
  init = function()
    local alpha = require('alpha')
    local dashboard = require('alpha.themes.dashboard')

    local config = {
      layout = {
        header,
        { type = 'padding', val = 2 },
        recent_files,
        { type = 'padding', val = 3 },
        buttons(dashboard),
      },
    }
    alpha.setup(config)
  end,
}

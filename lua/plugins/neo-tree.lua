return {
  'nvim-neo-tree/neo-tree.nvim',
  branch = 'v3.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
    'MunifTanjim/nui.nvim',
  },
  opts = {
    filesystem = {
      filtered_items = {
        hide_dotfiles = false,
        hide_gitignored = false,
      },
    },
    window = {
      mappings = {
        ['^'] = 'navigate_up',
        ['<bs>'] = 'close_node',
      },
    },
  },
  init = function()
    -- Close neo-tree if it is the last window
    -- vim.cmd([[
    --     autocmd WinEnter * if winnr('$') == 1 && &filetype == 'neo-tree' | q | endif
    --   ]])
  end,
}

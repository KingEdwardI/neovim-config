return {
  'akinsho/bufferline.nvim',
  config = function()
    require('bufferline').setup({
      options = {
        -- close_command = function(n) LazyVim.ui.bufremove(n) end,
        -- right_mouse_command = function(n) LazyVim.ui.bufremove(n) end,
        diagnostics = 'nvim_lsp',
        always_show_bufferline = true,
        separator_style = 'slant',
        hover = {
          enabled = true,
          delay = 200,
          reveal = { 'close' },
        },
        offsets = {
          {
            filetype = 'neo-tree',
            text = 'Neo-tree',
            highlight = 'Directory',
            text_align = 'left',
          },
        },
        diagnostics_indicator = function(
          _, -- count,
          _, -- level,
          diagnostics_dict,
          _ --context
        )
          local s = ' '
          for e, n in pairs(diagnostics_dict) do
            local sym = e == 'error' and ' '
              or (e == 'warning' and ' ' or '')
            s = s .. n .. sym
          end
          return s
        end,
      },
      highlights = {
        background = { bg = '#2d2d2d' },
        buffer_selected = { bg = '#2d2d2d' },
        buffer_visible = { bg = '#2d2d2d' },
        close_button = { bg = '#2d2d2d' },
        close_button_selected = { bg = '#2d2d2d' },
        close_button_visible = { bg = '#2d2d2d' },
        diagnostic = { bg = '#2d2d2d' },
        diagnostic_selected = { bg = '#2d2d2d' },
        diagnostic_visible = { bg = '#2d2d2d' },
        duplicate = { bg = '#2d2d2d' },
        duplicate_selected = { bg = '#2d2d2d' },
        duplicate_visible = { bg = '#2d2d2d' },
        error = { bg = '#2d2d2d' },
        error_diagnostic = { bg = '#2d2d2d' },
        error_diagnostic_selected = { bg = '#2d2d2d' },
        error_diagnostic_visible = { bg = '#2d2d2d' },
        error_selected = { bg = '#2d2d2d' },
        error_visible = { bg = '#2d2d2d' },
        fill = {},
        hint = { bg = '#2d2d2d' },
        hint_diagnostic = { bg = '#2d2d2d' },
        hint_diagnostic_selected = { bg = '#2d2d2d' },
        hint_diagnostic_visible = { bg = '#2d2d2d' },
        hint_selected = { bg = '#2d2d2d' },
        hint_visible = { bg = '#2d2d2d' },
        indicator_selected = { bg = '#2d2d2d' },
        indicator_visible = { bg = '#2d2d2d' },
        info = { bg = '#2d2d2d' },
        info_diagnostic = { bg = '#2d2d2d' },
        info_diagnostic_selected = { bg = '#2d2d2d' },
        info_diagnostic_visible = { bg = '#2d2d2d' },
        info_selected = { bg = '#2d2d2d' },
        info_visible = { bg = '#2d2d2d' },
        modified = { bg = '#2d2d2d' },
        modified_selected = { bg = '#2d2d2d' },
        modified_visible = { bg = '#2d2d2d' },
        numbers = { bg = '#2d2d2d' },
        numbers_selected = { bg = '#2d2d2d' },
        numbers_visible = { bg = '#2d2d2d' },
        offset_separator = { bg = '#2d2d2d' },
        pick = {},
        pick_selected = {},
        pick_visible = {},
        separator = { bg = '#2d2d2d' },
        separator_selected = { bg = '#2d2d2d' },
        separator_visible = { bg = '#2d2d2d' },
        tab = {},
        tab_close = {},
        tab_selected = {},
        tab_separator = {},
        tab_separator_selected = {},
        trunc_marker = { bg = '#2d2d2d' },
        warning = { bg = '#2d2d2d' },
        warning_diagnostic = { bg = '#2d2d2d' },
        warning_diagnostic_selected = { bg = '#2d2d2d' },
        warning_diagnostic_visible = { bg = '#2d2d2d' },
        warning_selected = { bg = '#2d2d2d' },
        warning_visible = { bg = '#2d2d2d' },
      },
    })
  end,
}
